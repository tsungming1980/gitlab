# frozen_string_literal: true

module Elastic
  module Latest
    class ProjectClassProxy < ApplicationClassProxy
      def elastic_search(query, options: {})
        if ::Elastic::DataMigrationService.migration_has_finished?(:migrate_projects_to_separate_index)
          options[:project_id_field] = :id
          options[:no_join_project] = true
        end

        options[:in] = %w[name^10 name_with_namespace^2 path_with_namespace path^9 description]

        query_hash = basic_query_hash(options[:in], query, options)

        filters = [{ terms: { _name: context.name(:doc, :is_a, es_type), type: [es_type] } }]

        context.name(:project) do
          if options[:namespace_id]
            filters << {
              terms: {
                _name: context.name(:related, :namespaces),
                namespace_id: [options[:namespace_id]].flatten
              }
            }
          end

          unless options[:include_archived]
            filters << {
              terms: {
                _name: context.name(:archived, false),
                archived: [false]
              }
            }
          end

          if options[:visibility_levels]
            filters << {
              terms: {
                _name: context.name(:visibility_level),
                visibility_level: [options[:visibility_levels]].flatten
              }
            }
          end

          if options[:project_ids]
            namespace = namespace_for_traversal_ids_filter(options)

            if namespace.present?
              prefix_filter = prefix_traversal_id_query(namespace, options)

              filters << prefix_filter unless prefix_filter == {}
              must_not = rejected_project_filter(namespace, options)
            else
              filters << {
                bool: project_ids_query(options[:current_user], options[:project_ids], options[:public_and_internal_projects], no_join_project: options[:no_join_project], project_id_field: options[:project_id_field])
              }
            end
          end

          query_hash[:query][:bool][:filter] ||= []
          query_hash[:query][:bool][:filter] += filters
          query_hash[:query][:bool][:must_not] = must_not if must_not
        end

        search(query_hash, options)
      end

      def namespace_for_traversal_ids_filter(options)
        return unless traversal_ids_flag_enabled?(options)
        return if should_use_project_ids_filter?(options)

        group = Group.find(options[:group_id])
        group if options[:current_user].authorized_groups.include?(group)
      end

      def traversal_ids_flag_enabled?(options)
        options[:current_user] &&
          Feature.enabled?(:advanced_search_project_traversal_ids_query, options[:current_user], type: :gitlab_com_derisk)
      end

      def prefix_traversal_id_query(namespace, options)
        ancestry_filter(options[:current_user], [namespace.elastic_namespace_ancestry], prefix: :traversal_ids)
      end

      def rejected_project_filter(namespace, options)
        project_ids_public_or_visibile_to_user = Project.public_or_visible_to_user(options[:current_user]).pluck_primary_key
        rejected_ids = namespace.all_project_ids_except(project_ids_public_or_visibile_to_user).pluck_primary_key

        return unless rejected_ids.any?

        {
          terms: {
            id: rejected_ids
          }
        }
      end

      # rubocop: disable CodeReuse/ActiveRecord
      def preload_indexing_data(relation)
        relation.includes(:project_feature, :route, :namespace, :catalog_resource)
      end
      # rubocop: enable CodeReuse/ActiveRecord
    end
  end
end
